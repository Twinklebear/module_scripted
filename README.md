# module\_scripted

An example module that uses the scripting functionality to expose new features
to the OSPRay scripted viewers. For now you'll need to be on the `will/module-scripting`
branch of OSPRay.

For example the `msMakeScene` function is exposed which creates a Spheres geometry
with 3 blue spheres. From a script or interactive console:

```javascript
var box = box3f()
var g = msMakeScene(r, box)
m.addGeometry(g)
m.commit()
setWorldBounds(box)
```

