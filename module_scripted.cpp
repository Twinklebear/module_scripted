#include <iostream>
#include <ospray/ospray.h>
#include <ospcommon/vec.h>
#include <ospcommon/box.h>

#ifdef OSPRAY_APPS_ENABLE_SCRIPTING
#include <OSPRayScriptHandler.h>
#endif

using namespace ospcommon;

#ifdef OSPRAY_APPS_ENABLE_SCRIPTING
namespace script {
  namespace cs = chaiscript;

  void move_camera(ospray::cpp::Camera camera) {
    camera.set("pos", vec3f(-12, 12, 50));
    camera.set("dir", vec3f(0) - vec3f(-12, 12, 50));
    camera.commit();
  }

  ospray::cpp::Geometry make_scene(ospray::cpp::Renderer renderer, box3f &bounds) {
    using namespace ospray::cpp;
    const std::array<float, 3*3> centers = {
      -1, -1, 0,
      1, -1, 0,
      1, 1, 0
    };
    Data centerData(centers.size(), OSP_FLOAT, centers.data());
    centerData.commit();
    Geometry spheres("spheres");
    spheres.set("bytes_per_sphere", static_cast<int>(3 * sizeof(float)));
    spheres.set("spheres", centerData);

    Material mat = renderer.newMaterial("OBJMaterial");
    mat.set("kd", vec3f(0.f, 0.f, 1.f));
    mat.set("ks", vec3f(0.6f));
    mat.commit();

    OSPMaterial mHandle = mat.handle();
    Data matData(1, OSP_OBJECT, &mHandle);
    matData.commit();
    spheres.set("radius", 0.5f);
    spheres.set("materialList", matData);
    spheres.commit();

    bounds = box3f(vec3f(-1.5, -1.5, -0.5), vec3f(1.5, 1.5, 0.5));
    return spheres;
  }

  void registerModule(cs::ChaiScript &engine) {
    engine.add(cs::fun(&make_scene), "msMakeScene");
    engine.add(cs::fun(&move_camera), "msMoveCamera");
  }
  void printModuleHelp() {
    std::cout
      << "==Scripted Module Help==\n"
         "  Functions:\n"
         "    Geometry msMakeScene(renderer, bounds):\n"
         "        Create a geometry with 3 blue spheres and return it\n"
         "        and the bounds of the world.\n\n"
         "    void msMoveCamera(camera):\n"
         "        Move the camera to (-12, 12, 50) looking at the origin.\n"
         "        You'll likely want to call refresh() after this function.\n";
         "  Globals:\n"
         "    msCoolValue: an int with value of 10, just a testing thing\n"
         "====\n";
  }
}
#endif

extern "C" void ospray_init_module_scripted() {
  std::cout << "#osp:module_scripted: loading 'scripted' module" << std::endl;
#ifdef OSPRAY_APPS_ENABLE_SCRIPTING
  ospray::script::register_module(script::registerModule, script::printModuleHelp);
#else
  std::cout << "#osp:module_scripted: Warning - OSPRAY_APPS_ENABLE_SCRIPTING is off,"
    << " this module has nothing to do" << std::endl;
#endif
}

